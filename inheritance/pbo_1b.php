<?php
class BangunDatar
{
    public $namaBangun;
    public function __construct($namaBangun)
    {
        $this->namaBangun = $namaBangun;
    }
    public function printNama()
    {
        echo "<br>Nama Bangun Datar : $this->namaBangun";
    }
}


class persegi extends bangunDatar{
    public $sisi;
    public function __construct($namaBangun, $sisi)
    {
        $this->namaBangun = $namaBangun;
        $this->sisi = $sisi;
    }
    public function keliling()
    {
        $keliling = 4 * $this->sisi;
        return $keliling;
    }
    public function luas()
    {
        $luas = $this->sisi*$this->sisi;
        return $luas;
    }

}

$persegi1 = new persegi("kotak", 2);
echo "keliling persegi adalah:".$persegi1->keliling();
echo "<br>";
echo "luas persegi adalah:".$persegi1->luas();
echo "<hr>";

class persegiPanjang extends bangunDatar{
    public $panjang;
    public $lebar;
    
    public function __construct($namaBangun, $panjang, $lebar)
    {
        $this->namaBangun = $namaBangun;
        $this->panjang = $panjang;
        $this->lebar = $lebar;
    }
    public function keliling(){
        $keliling = 2*($this->panjang+$this->lebar);
        return $keliling;
    }
    public function luas(){
        $luas =  $this->panjang*$this->lebar;
        return $luas;
    }
}
$persegiPanjang1=new persegiPanjang("persegi panjang", 2,3);
echo "keliling persegi panjang adalah:".$persegiPanjang1->keliling();
echo "<br>";
echo "keliling persegi panjang adalah:".$persegiPanjang1->luas();
echo "<hr>";

class segitiga extends bangunDatar{
    public $alas;
    public $tinggi;
    public $sisiMiring;

    public function __construct($alas, $tinggi)
    {
        $this->alas = $alas;
        $this->tinggi = $tinggi;
    }
    public function keliling(){
        $keliling = 3*$this->alas;
        return $keliling;
    }
    public function luas(){
        $luas =  ($this->alas*$this->tinggi)/2;
        return $luas;
    }
}
$segi3 = new segitiga(3, 5 );
echo "keliling segitiga adalah:". $segi3 ->keliling();
echo "<br>";
echo "luas segitiga adalah:". $segi3 ->luas();
echo "<hr>";

class lingkaran extends bangunDatar{
    public $phi = 3.14;
    public $jari_jari;

    public function __construct($namaBangun, $jari_jari)
    {
        $this->namaBangun = $namaBangun;
        $this->jari_jari = $jari_jari;
    }
    public function keliling()
    {
        $keliling = 2*$this->phi*$this->jari_jari;
        return $keliling;
    }
    public function luas()
    {
        $luas = $this->phi*$this->jari_jari*$this->jari_jari;
        return $luas;
    }

}
$lingkaran1 = new lingkaran("lingkaran", 7 );
echo "keliling lingkaran adalah:". $lingkaran1 ->keliling();
echo "<br>";
echo "luas lingkaran adalah:". $lingkaran1 ->luas();
echo "<hr>";

class trapesium extends bangunDatar{
    public $sisi_atas;
    public $sisi_bawah;
    public $sisi_miring;
    public $tinggi;

    public function __construct($namaBangun, $sisi_atas, $sisi_bawah, $tinggi, $sisi_miring)
    {
        $this->namaBangun = $namaBangun;
        $this->sisi_atas = $sisi_atas;
        $this->sisi_bawah = $sisi_bawah;
        $this->tinggi = $tinggi;
        $this->sisi_miring = $sisi_miring;
    }
    public function keliling()
    {
        $keliling = $this->sisi_atas + $this->sisi_bawah + $this->tinggi + $this->sisi_miring;
        return $keliling;
    }
    public function luas()
    {
        $luas = ($this->sisi_atas + $this->sisi_bawah)*$this->tinggi/2;
        return $luas;
    }

}
$trapesium1 = new trapesium("trapesium", 4,6,5,2);
echo "keliling trapesium adalah:". $trapesium1 ->keliling(2,4,3,3);
echo "<br>";
echo "luas trapesium adalah:". $trapesium1 ->luas(2,4,1.5);
echo "<hr>";

class jajarGenjang extends bangunDatar{
    public $sisi_datar;
    public $sisi_miring;
    public $tinggi;

    public function __construct($namaBangun, $sisi_datar, $sisi_miring, $tinggi)
    {
        $this->namaBangun = $namaBangun;
        $this->sisi_datar = $sisi_datar;
        $this->tinggi = $tinggi;
        $this->sisi_miring = $sisi_miring;
    }
    public function keliling()
    {
        $keliling = 2*$this->sisi_datar + 2*$this->sisi_miring;
        return $keliling;
    }
    public function luas()
    {
        $luas = $this->sisi_datar*$this->tinggi;
        return $luas;
    }

}
$jajarGenjang1 = new jajarGenjang("jajar genjang", 5,3,4  );
echo "keliling jajar Genjang adalah:". $jajarGenjang1 ->keliling();
echo "<br>";
echo "luas jajar Genjang adalah:". $jajarGenjang1 ->luas();
echo "<hr>";

class belahKetupat extends BangunDatar
{
    public $panjang;
    public $lebar;

    public function __construct($namaBangun, $isiPanjang, $isiLebar)
    {
        $this->namaBangun = $namaBangun;
        $this->panjang = $isiPanjang;
        $this->lebar = $isiLebar;
    }
    public function keliling()
    {
        $keliling = 2*($this->panjang + $this->lebar);
        return $keliling;
    }
    public function luas()
    {
        $luas = $this->panjang * $this->lebar;
        return $luas;
    }
}
$belahKetupat1 = new belahKetupat("ketupat", 5,4);
echo "keliling belah ketupat adalah:". $belahKetupat1 ->keliling();
echo "<br>";
echo "luas belah ketupat adalah:". $belahKetupat1 ->luas();
echo "<hr>";
?>