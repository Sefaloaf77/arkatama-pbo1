<?php
class CatFish{
    var $numberOfLegs = "0";
    var $canFly = false;
    var $foodClassification = "omnivora";
    var $canSwim = true;
    var $respirationOrgan = "insang";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
$ehe = new CatFish();
var_dump($ehe);
class BettaFish{
    var $numberOfLegs = "0";
    var $canFly = false;
    var $foodClassification = "omnivora";
    var $canSwim = true;
    var $respirationOrgan = "insang";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Crocodile{
    var $numberOfLegs = "4";
    var $canFly = false;
    var $foodClassification = "karnivora";
    var $canSwim = true;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Aligator{
    var $numberOfLegs = "4";
    var $canFly = false;
    var $foodClassification = "karnivora";
    var $canSwim = true;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Lizard{
    var $numberOfLegs = "4";
    var $canFly = false;
    var $foodClassification = "serangga";
    var $canSwim = false;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Snake{
    var $numberOfLegs = "0";
    var $canFly = false;
    var $foodClassification = "karnivora";
    var $canSwim = false;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Swan{
    var $numberOfLegs = "2";
    var $canFly = true;
    var $foodClassification = "herbivora";
    var $canSwim = true;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Chicken{
    var $numberOfLegs = "2";
    var $canFly = true;
    var $foodClassification = "omnivora";
    var $canSwim = false;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Swallow{
    var $numberOfLegs = "2";
    var $canFly = true;
    var $foodClassification = "insektivora";
    var $canSwim = false;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Duck{
    var $numberOfLegs = "2";
    var $canFly = true;
    var $foodClassification = "omnivora";
    var $canSwim = true;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Human{
    var $numberOfLegs = "2";
    var $canFly = false;
    var $foodClassification = "omnivora";
    var $canSwim = true;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Whale{
    var $numberOfLegs = "0";
    var $canFly = false;
    var $foodClassification = "karnivora";
    var $canSwim = true;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Dolphine{
    var $numberOfLegs = "0";
    var $canFly = false;
    var $foodClassification = "karnivora";
    var $canSwim = true;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Bat{
    var $numberOfLegs = "2";
    var $canFly = true;
    var $foodClassification = "omnivora";
    var $canSwim = false;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
class Tiger{
    var $numberOfLegs = "4";
    var $canFly = false;
    var $foodClassification = "karnivora";
    var $canSwim = false;
    var $respirationOrgan = "paru-paru";

    public function eat(){
        echo "makan";
    }
    public function run(){
        echo "lariii";
    }
    public function swim(){
        echo "renang";
    }
    public function fly(){
        echo "mumbul";
    }
    public function cry(){
        echo "nangis";
    }
}
?>