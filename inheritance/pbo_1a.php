<?php 
class Animal{
    public $numberOfLegs;
    public $foodClassification;
    public $respirationOrgan;

    public function __construct($numberOfLegs, $foodClassification, $respirationOrgan)
    {
        $this->numberOfLegs = $numberOfLegs;
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
    }
    
    public function eat(){
        return "$this->foodClassification yang suka mamam";
    }
    public function cry(){
        return "bersuara";
    }
}

class Bird extends Animal{
    public $canFly;
    
    public function fly(){
        return "bisa terbang nihh";
    }
    
}
$bebek = new Bird(0,"omnivora","paru-paru");
echo $bebek->fly();
echo "<br>";
echo $bebek->eat();
echo "<br>";
echo $bebek->cry();
echo "<br>";
echo $bebek->numberOfLegs;

class Fish extends Animal{
    public $canSwim;
    
    public function swim(){
        return "bisa renang nihh";
    }
}

class Amphibians extends Animal{
    public $canSwim;

    public function swim(){
        return "bisa renang nihh";
    }
}

class Mammals extends Animal{
    public function run(){
        return "bisa lari kenceng";
    }
    public function swim(){
        return "bisa renang nihh";
    }
    public function fly(){
        return "bisa terbang nihh";
    }
}

class reptile extends Animal{
    public function run(){
        return "bisa lari kenceng";
    }
    public function swim(){
        return "bisa renang nihh";
    }
}

?>